CREATE DATABASE IF NOT EXISTS sugo_cakephp_db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE sugo_cakephp_db;
CREATE TABLE IF NOT EXISTS  users (
    id        SERIAL       PRIMARY KEY,
    email     VARCHAR(255) UNIQUE NOT NULL, 
    password  VARCHAR(1024) NOT NULL,
    lastname  VARCHAR(255) NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    lastKana  VARCHAR(255) NULL,
    firstKana VARCHAR(255) NULL,
    role      VARCHAR(20)  NOT NULL,
    created   TIMESTAMP    NOT NULL DEFAULT NOW(), 
    modified  TIMESTAMP    DEFAULT NOW()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
CREATE TABLE sessions (
    id char(40) NOT NULL,
    data text,
    expires INT(11) NOT NULL,
    PRIMARY KEY  (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;