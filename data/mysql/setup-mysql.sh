#!/bin/bash
rootpassword='Sugo-0000'
initdbname='sugo_cakephp_db'
echo 'Step1. Disabling and removing partially MariaDB'
yum remove -y mariadb-libs MariaDB-common MariaDB-compat MariaDB-server
yum repolist enabled | grep "mariadb.*"
echo 'Step2. Removing completely mariadb' 
yum remove -y mariadb mariadb-server
echo 'Step3. Removing mysql'
rm /etc/my.cnf
yum remove -y mysql-server mysql-libs mysql-devel mysql
echo 'Step4. Installing mysql'
yum -y install http://dev.mysql.com/get/mysql57-community-release-el6-7.noarch.rpm
yum -y install mysql-community-server
echo 'Step5. Creating /etc/sysconfig/network if not exist'
networkfile=/etc/sysconfig/network
if [ -e $networkfile ]; then
    echo "$networkfile found"
else
    echo "NETWORKING=yes" >$networkfile
fi
echo 'Step6. Starting mysql'
chkconfig mysqld on
service enable mysqld
service mysqld start
echo 'Step7. Changing root password'
oldpassword=$( grep 'temporary.*root@localhost' /var/log/mysqld.log | tail -n 1 |  sed 's/.*root@localhost: //' )
mysqladmin -u root --password=${oldpassword} password $rootpassword
wget https://gitlab.com/sugo-open-group/amazonlinux-cakephp3-basic/blob/master/data/mysql/sugo_cakephp_db.sql
mysql -u root -p $rootpassword $initdbname < ./sugo_cakephp_db.sql